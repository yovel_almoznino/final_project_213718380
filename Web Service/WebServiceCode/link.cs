﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
namespace WebService
{
    public class link
    {
        private static string connectionString = "server=localhost;user id=root;password=josh17rog;persistsecurityinfo=True;database=links;sslmode=None";
        public static List<string> names()
        {

            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT name FROM links.name_link;";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            List<string> name = new List<string>();
            while (r.Read())
            {
                name.Add(r.GetValue(0).ToString());
            }
            con.Close();
            return name;
        }
        public static string getLink(string name)
        {
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT link FROM links.name_link WHERE name='{name}';";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            string link = r.GetValue(0).ToString();
            con.Close();
            return link;
        }
        public static string getBrowse(string name)
        {
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT browse FROM links.name_link WHERE name='{name}';";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            string link = r.GetValue(0).ToString();
            con.Close();
            return link;
        }

    }
}