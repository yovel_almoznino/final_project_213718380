﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public partial class AdminLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable())
            {
                Server.Transfer("Error.aspx");
            }
            if (Page.IsPostBack)
            {
                string input = password.Text;
                password.TextMode = TextBoxMode.Password;
                password.Attributes.Add("value", input);
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string userName = Username.Text;
            string Password = password.Text;
            string ID = id.Text;

            CurrentUser user = Admins.Login(userName, ID, Password);
            if(user != null)
            {
                Session["user"] = user;
                Server.Transfer("DeleteUsers.aspx");
            }
        }

        
        /*
         * Image button of showing password clicked
         */
        protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
        {
            string input = password.Text;
            if (ImageButton1.ImageUrl == DB.show)
            {
                ImageButton1.ImageUrl = DB.hide;
                password.TextMode = TextBoxMode.SingleLine;
                password.Text = input;
            }
            else
            {
                ImageButton1.ImageUrl = DB.show;
                password.TextMode = TextBoxMode.Password;
                password.Text = input;
            }
        }
    }
}