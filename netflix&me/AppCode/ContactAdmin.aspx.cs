﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public partial class ContactAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable())
            {
                Server.Transfer("Error.aspx");
            }
            if (Session["user"] == null)
            {
                Server.Transfer("Login.aspx");
            }
            CurrentUser currentUser = (CurrentUser)Session["user"];
            username.Text = "Hello " + currentUser.getUsername();
        }

        protected void send_Click(object sender, EventArgs e)
        {
            CurrentUser currentUser = (CurrentUser)Session["user"];
            Mail.sendMsg(Users.getMailById(currentUser.getID()), body.Text, "Copy of your message");
            Mail.sendMsg("netflix.and.me4u@gmail.com", body.Text, subject.Text + " - " + currentUser.getUsername() + "-" + currentUser.getID());
            Server.Transfer("ContactAdmin.aspx");
        }
        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue != "ContactAdmin")
                Server.Transfer(DropDownList1.SelectedValue);
        }
    }
}