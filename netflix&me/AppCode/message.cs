﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Netflix
{
    public class message
    {
        private string author;
        private string msg;

        public string getAuthor()
        {
            return this.author;
        }
        public void setAutor(string author)
        {
            this.author = author;
        }
        public string getMsg()
        {
            return this.msg;
        }
        public void setMsg(string msg)
        {
            this.msg = msg;
        }

        public message(string author, string message)
        {
            this.msg = message;
            this.author = author;
        }
    }
}