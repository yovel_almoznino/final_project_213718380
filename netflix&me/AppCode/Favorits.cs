﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public class Favorits
    {
        public static List<series> GetSeries(string userID)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT DISTINCT serieses.name
                                FROM saveseries
                                INNER JOIN serieses ON saveseries.seriesID=serieses.id
                                WHERE userID='{userID}';";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            List<series> series = new List<series>();
            while (r.Read())
            {
                series.Add(new series(r.GetValue(0).ToString()));
            }
            con.Close();
            return series;
        }
        public static List<movie> GetMovies(string userID)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT DISTINCT movies.name
                                FROM savemovies
                                INNER JOIN movies ON savemovies.movieID=movies.id
                                WHERE userID='{userID}';";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            List<movie> movies = new List<movie>();
            while (r.Read())
            {
                movies.Add(new movie(r.GetValue(0).ToString()));
            }
            con.Close();
            return movies;
        }
    }
}