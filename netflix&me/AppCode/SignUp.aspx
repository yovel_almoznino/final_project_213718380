﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SignUp.aspx.cs" Inherits="Netflix.SignUp" %>

<!DOCTYPE HTML>
<!--
	Colorized by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
<head runat="server">
	<title>Netflix&&Me</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<link href='https://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
	<!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="js/skel.min.js"></script>
	<script src="js/skel-panels.min.js"></script>
	<script src="js/init.js"></script>
	<noscript>
		<link rel="stylesheet" href="css/skel-noscript.css" />
		<link rel="stylesheet" href="css/style.css" />
		<link rel="stylesheet" href="css/style-desktop.css" />
	</noscript>
	<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
</head>
	<body>
		<form id="form1" runat="server">
			<!-- Header -->
			<div id="header">
				<div id="logo-wrapper">
					<div class="container">

						<!-- Logo -->
						<div id="logo">
							<h1><a href="#">Netflix && Me</a></h1>
							<span>Just for You</span>
						</div>

					</div>
				</div>
				<div class="container">
					<!-- Nav -->
					<nav id="nav">
						<ul>
							
							<li><a href="Login.aspx">Login</a></li>
							<li class="active"><a href="SignUp.aspx">Sign Up</a></li>
							<li ><a href="AdminLogin.aspx">Admins Login</a></li>
						</ul>
					</nav>
				</div>
			</div>
			<!-- Header -->
			<!-- Banner -->
			<div id="banner">
				<div class="container">
				</div>
			</div>
			<!-- /Banner -->
			<!-- Main -->
			<div id="main">

				<!-- Main Content -->
				<div class="container">
					<div class="row">
						<div class="12u">
							<section>
								<header>
									<h2>Sign Up</h2>
								</header>
								<div>
									Username:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="username" runat="server" Width="600" BorderStyle="Solid"></asp:TextBox>
									<br />
									Password:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="password" runat="server" Width="600" BorderStyle="Solid" TextMode="Password"></asp:TextBox>									    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="https://pngimage.net/wp-content/uploads/2018/06/view-png-icon-6.png" Height="22px" Width="19px" ImageAlign="Middle" OnClick="ImageButton1_Click" />

									<br />
									Email:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="mail" runat="server" Width="600" BorderStyle="Solid"></asp:TextBox>
									<br />
									ID:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="id" runat="server" Width="600" BorderStyle="Solid"></asp:TextBox>
									<br />
									birthday:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="date" runat="server" Width="600px" BorderStyle="Solid" Text="YYYY-MM-DD"></asp:TextBox>
									<br />
									phone Number:<asp:TextBox ID="phone" runat="server" Width="600" BorderStyle="Solid"></asp:TextBox>
								    <br />
                                    <br />
								</div>
								<asp:Button ID="Button1" runat="server" Text="Sign Up!" OnClick="Button1_Click" CssClass="button" />
								<p>
									<asp:Label ID="Res" runat="server" ForeColor="Red"></asp:Label>
								</p>
							</section>
						</div>
					</div>

				</div>
				<!-- /Main Content -->

			</div>
			<!-- /Main -->
			<!-- Footer -->
			<div id="footer">
				<div class="container">
					<div class="row">
						
						
					</div>


				</div>
			</div>
			<!-- /Footer -->
			<!-- Copyright -->

			</form>
	</body>
</html>

