﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public class Users
    {
        public static bool ExictByName(string userName)
        {
            string connectionString = DB.connection;

            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT        userName
                                FROM            users
                                WHERE        (userName = '{userName}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            if (r.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }
        public static bool ExictByID(string ID)
        {
            string connectionString = DB.connection;

            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT        ID
                                FROM            users
                                WHERE        (ID = '{ID}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            if (r.HasRows)
            {
                con.Close();
                return true;
            }
            else
            {
                con.Close();
                return false;
            }
        }
        public static CurrentUser SignUP(string userName, string ID, string password,string mail, string birthDate, string phoneNum)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"INSERT INTO users VALUES('{ID}', '{userName}', '{password}', '{mail}', '{phoneNum}', '{birthDate}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            if (ExictByID(ID))
            {
                con.Close();
                return null;
            }
            int r = cmd.ExecuteNonQuery();
            if(r == 1)
            {
                con.Close();
                return new CurrentUser(password, userName, ID);
            }
            con.Close();
            return null;
        }
        public static CurrentUser Login(string username, string Password)
        {
            string connectionString = DB.connection;

            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT        password,ID
                                FROM            users
                                WHERE        (userName = '{username}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            int count = 0;
            while (r.Read())
            {
                count++;
            }
            if (!ExictByName(username))
            {
                con.Close();
                return null;
            }
            string pass = r[0].ToString();
            string ID = r[1].ToString();
            if(pass == Password)
            {
                con.Close();
                return new CurrentUser(Password,username,ID);
            }
            else
            {
                con.Close();
                return null;
            }
           
        }
        public static CurrentUser FindByName(string username)
        {
            string connectionString = DB.connection;

            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT        password,ID
                                FROM            users
                                WHERE        (userName = '{username}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            int count = 0;
            while (r.Read())
            {
                count++;
            }
            if (!ExictByName(username))
            {
                con.Close();
                return null;
            }
            string pass = r[0].ToString();
            string ID = r[1].ToString();
            con.Close();
            return new CurrentUser(pass, username, ID);

        }
        public static CurrentUser updatePass(string id,string password, string username)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"update users
                                set password='{password}'
                                where ID = '{id}'; ";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            if (!ExictByID(id))
            {
                con.Close();
                return null;
            }
            int r = cmd.ExecuteNonQuery();
            if (r == 1)
            {
                con.Close();
                return new CurrentUser(password, username, id);
            }
            con.Close();
            return null;
        }
        public static void ChangePass(string id, string password)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"update users
                                set password='{password}'
                                where ID = '{id}'; ";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            if (!ExictByID(id))
            {
                con.Close();
                return;
            }
            int r = cmd.ExecuteNonQuery();
            
            con.Close();
        }
        public static CurrentUser updateUsername(string id, string password, string username)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"update users
                                set userName='{username}'
                                where ID = '{id}'; ";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            if (!ExictByID(id))
            {
                con.Close();
                return null;
            }
            int r = cmd.ExecuteNonQuery();
            if (r == 1)
            {
                con.Close();
                return new CurrentUser(password, username, id);
            }
            con.Close();
            return null;
        }
        public static void updateMail(string id, string mail)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"update users
                                set mail='{mail}'
                                where ID = '{id}'; ";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int r = cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void updatePhoneNum(string id, string phone)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"update users
                                set phoneNum='{phone}'
                                where ID = '{id}'; ";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int r = cmd.ExecuteNonQuery();
            con.Close();
        }
        public static void updatephoto(string id, byte[] photo)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            
            string query = "update users set image=@image where ID=@ID;";
            using (MySqlCommand cmd = new MySqlCommand(query))
            {
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@image", photo);
                cmd.Parameters.AddWithValue("@ID", id);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }

        }
        public static string getMailById(string id)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT        mail
                                FROM            users
                                WHERE        (ID = '{id}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            string mail = r.GetValue(0).ToString();
            con.Close();
            return mail;
        }
        public static byte[] getImageById(string id)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT        image
                                FROM            users
                                WHERE        (ID = '{id}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            byte[] image = { 0 };
            if (!r.IsDBNull(0))
            {
                image = (byte[])r["image"];
            }
            con.Close();

            return image;
        }
        public static string getPhoneById(string id)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT        phoneNum
                                FROM            users
                                WHERE        (ID = '{id}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            string phoneNum = r.GetValue(0).ToString();
            con.Close();
            return phoneNum;
        }
        public static string getPassById(string id)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT        password
                                FROM            users
                                WHERE        (ID = '{id}');";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            string pass = r.GetValue(0).ToString();
            con.Close();
            return pass;
        }
    }
}