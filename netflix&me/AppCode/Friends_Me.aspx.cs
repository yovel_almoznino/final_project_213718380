﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
namespace Netflix
{
    public partial class Friends_Me : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!handler.IsAvailable())
            {
                Server.Transfer("Error.aspx");
            }
            if (Session["user"] == null)
            {
                Server.Transfer("Login.aspx");
            }
            CurrentUser currentUser = (CurrentUser)Session["user"];
            username.Text = "Hello " + currentUser.getUsername();
            friendName.Text = DropDownList1.SelectedValue;
            content.Text = "";
            photos.Text = "";
            if (!Page.IsPostBack)
            {
                List<string> friendsNames = friends.getFriends(currentUser.getID(), currentUser.getUsername());
                ListItem l = new ListItem("Choose Friend", "0");
                DropDownList1.Items.Add(l);
                foreach (string name in friendsNames)
                {
                    l = new ListItem(name, name);
                    DropDownList1.Items.Add(l);
                }
            }
            
        }
        //What the current user and his selected friend has in common
        protected void Match_Click(object sender, EventArgs e)
        {
            
            content.Text = "";
            photos.Text = "";
            CurrentUser currentUser = (CurrentUser)Session["user"];
            CurrentUser friend = Users.FindByName(DropDownList1.SelectedValue);
            List<string> friendsNames = friends.getFriends(currentUser.getID(), currentUser.getUsername());
            if (friendsNames.Contains(friendName.Text))
            {
                List<movie> m = lists.getFriendsMatchMovies(currentUser.getID(), friend.getID());
                List<series> s = lists.getFriendsMatchSeries(currentUser.getID(), friend.getID());
                foreach (movie movies in m)
                {
                    content.Text += "<br/>About the movie:<br/>Description:" + movies.getDes() + "<br/>" + "Minutes long:" + movies.getLong() + "<br/>" + "Relesed on:" + movies.getRele() + " <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";

                    photos.Text += movies.getName() + " <br/>  " + "<img src=" + movies.getImage() + " height='300' width='280'>" + "<br/><br/><br/>";
                }
                foreach (series series in s)
                {
                    content.Text += "<br/>About the series:<br/>Description:" + series.getDes() + "<br/>" + "Num of episodes:" + series.getLong() + "<br/>" + "Relesed on:" + series.getRele() + " <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";

                    photos.Text += series.getName() + " <br/>  " + "<img src=" + series.getImage() + " height='300' width='280'>" + "<br/><br/><br/>";

                }
            }
            else
            {
                content.Text = "This user is not on your friends list!";
            }
            
        }
        //The friend favourits
        protected void Fav_Click(object sender, EventArgs e)
        {
            content.Text = "";
            photos.Text = "";
            CurrentUser currentUser = (CurrentUser)Session["user"];
            CurrentUser friend = Users.FindByName(DropDownList1.SelectedValue);
            List<string> friendsNames = friends.getFriends(currentUser.getID(), currentUser.getUsername());
            if (friendsNames.Contains(friendName.Text))
            {
                List<movie> m = Favorits.GetMovies(friend.getID());
                List<series> s = Favorits.GetSeries(friend.getID());
                foreach (movie movies in m)
                {
                    content.Text += "<br/>About the movie:<br/>Description:" + movies.getDes() + "<br/>" + "Minutes long:" + movies.getLong() + "<br/>" + "Relesed on:" + movies.getRele() + " <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";

                    photos.Text += movies.getName() + " <br/>  " + "<img src=" + movies.getImage() + " height='300' width='280'>" + "<br/><br/><br/>";
                }
                foreach (series series in s)
                {
                    content.Text += "<br/>About the series:<br/>Description:" + series.getDes() + "<br/>" + "Num of episodes:" + series.getLong() + "<br/>" + "Relesed on:" + series.getRele() + " <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>";

                    photos.Text += series.getName() + " <br/>  " + "<img src=" + series.getImage() + " height='300' width='280'>" + "<br/><br/><br/>";

                }
            }
            else
            {
                content.Text = "This user is not on your friends list!";
            }
        }

        protected void changed(object sender, EventArgs e)
        {
            friendName.Text = DropDownList1.SelectedValue;
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(DropDownList2.SelectedValue != "Friends_Me.aspx")
                Server.Transfer(DropDownList2.SelectedValue);
        }
    }
}