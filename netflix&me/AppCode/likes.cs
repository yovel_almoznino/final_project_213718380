﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;

namespace Netflix
{
    public class likes
    {
        public static bool addLikeSeries(string name, string userID)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT id FROM serieses
                                WHERE name='{name}';";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            object[] vals = new object[1];
            r.GetValues(vals);
            int id = ((int)vals[0]);

            con.Close();
            con = new MySqlConnection(connectionString);
            SqlQuer = $@"INSERT INTO saveseries (`like`, `seriesID`, `userId`)
                                VALUES ('1', '{id}', '{userID}');";
            cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
            if (res == 0)
            {
                return false;
            }
            return true;
            
        }

        public static bool addLikeMovies(string name, string userID)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT id FROM movies
                                WHERE name='{name}';";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            object[] vals = new object[1];
            r.GetValues(vals);
            int id = ((int)vals[0]);

            con.Close();
            con = new MySqlConnection(connectionString);
            SqlQuer = $@"INSERT INTO savemovies (`like`, `movieID`, `userId`)
                                VALUES ('1', '{id}', '{userID}');";
            cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
            if (res == 0)
            {
                return false;
            }
            return true;
            
        }
        public static bool deleteLikeSeries(string name, string userID)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT id FROM serieses
                                WHERE name='{name}';";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            object[] vals = new object[1];
            r.GetValues(vals);
            int id = ((int)vals[0]);

            con.Close();
            con = new MySqlConnection(connectionString);
            SqlQuer = $@"DELETE FROM saveseries WHERE userID='{userID}' AND seriesID={id};";
            cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
            if (res == 0)
            {
                return false;
            }
            return true;

        }
        public static bool deleteLikeMovies(string name, string userID)
        {
            string connectionString = DB.connection;
            MySqlConnection con = new MySqlConnection(connectionString);
            string SqlQuer = $@"SELECT id FROM movies
                                WHERE name='{name}';";
            MySqlCommand cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            MySqlDataReader r = cmd.ExecuteReader();
            r.Read();
            object[] vals = new object[1];
            r.GetValues(vals);
            int id = ((int)vals[0]);

            con.Close();
            con = new MySqlConnection(connectionString);
            SqlQuer = $@"DELETE FROM savemovies WHERE userID='{userID}' AND movieID={id};";
            cmd = new MySqlCommand(SqlQuer, con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
            if (res == 0)
            {
                return false;
            }
            return true;

        }
    }

    
}
